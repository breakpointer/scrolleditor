# scrolleditor

[![Build Status](https://drone.io/bitbucket.org/michaelb/scrolleditor/status.png)](https://drone.io/bitbucket.org/michaelb/scrolleditor/latest)

-------------------------

**Not much to see here.**

* CLI version: http://bitbucket.org/michaelb/scrolleditor/

This is the GUI front-end for the very immature Scroll Editor, a
work-in-progress free software document processing system.

*Scroll is very WIP and not ready for public usage. If you really, really want
to dig in keep on reading.*

## Getting set up

To get a basic demo running, follow the following steps:

0. Check out this repo somewhere

1. First, install a recent version of `node.js`, along with a recent version of
npm.
    - The supported version is: `5.7` (same as ran by test server)
    - You might consider using `nvm` to easily install a version that is appropriate
        for your OS and architecture https://github.com/creationix/nvm
    - If you used `nvm`, remember to do `nvm use 5` to activate before continuing

2. Install dependencies by running `npm install -d`
3. Optionally, double check there's nothing wrong by running the unit tests
with `npm test`
4. Now, `git clone` the example workspace in some location, such as `scrollexamplews`

    * The example workspace is here: http://bitbucket.org/michaelb/scrollexamplews

## Playing around with very-WIP GUI

To launch the GUI loading the example workspace, type:

```
npm run start -- ./scrollexamplews/
```

There's not a lot you can do in the GUI in the present form, and many things
are glitchy.  Saving is disabled, and the various IDE aspects (to edit tags,
styles, structure, and so on), is totally unfinished, so only a very rough
version of the document editing is available.


## Image

![screenshot](https://bitbucket.org/michaelb/scrolleditor/raw/HEAD/screenshot.png)
