'use strict';

const ScrollObjectEditor = require('../../lib/ScrollObjectEditor');

class Tag extends ScrollObjectEditor {
    constructor(...args) {
        super(...args);
        this.setup_events();
    }

    setup_events() {
        this.on('save', new_value => {
            this.scrollobj.meta.contents = new_value;
        });
    }

    get_opts() {
        return {
            info: this.scrollobj.info,
            contents: this.scrollobj.meta.contents,
            tag_name: this.scrollobj.name,
        };
    }

    get tagname() {
        // Name of the channel this should use
        return 'tag';
    }
}

module.exports = Tag;
