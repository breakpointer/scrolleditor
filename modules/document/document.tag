<block-editor>
		<textarea
				id="editing"
				uuid={opts.uuid}
				value={opts.source} />
</block-editor>

<fragment-editor>
		<span></span>
		<script>
				'use strict';
				updateContent() {
						let html = [];
						const blocks = this.opts.blocks;
						for (const block of blocks) {
								if (block.editing) {
										html.push(...[
												'<textarea id="editing" uuid="', block.id, '">',
														block.source,
												'</textarea>'
										]);
								} else {
										html.push(block.preview);
								}
						}
						this.root.innerHTML = html.join('');
						// this.root.innerHTML = opts.content;
				}

				this.on('update', () => {
						this.updateContent();
				});

				this.updateContent();
		</script>
</fragment-editor>

<document>
		<div>
				<style scoped>
						bk {
								-webkit-user-select: text;
								user-select: text;
								cursor: text;
						}

						bk,
						textarea {
								display: block;
						}
						/* margin: 2px; */

						textarea,
						input {
								/*border: 1px gray solid;*/
								/*display: inline;*/
								/*font-family: serif;*/
								/* For Webkit, disable resize, since it is handled automatically... */
								resize: none;
								/*
								-moz-box-shadow:    0px 0px 2px 1px #999;
								-webkit-box-shadow: 0px 0px 2px 1px #999;
								box-shadow:         0px 0px 2px 1px #999;
								-moz-border-radius: 1px;
								-webkit-border-radius: 1px; 
								*/
								/*box-shadow: -1px 0px 10px 0px #000, 1px 0px 10px 0px #000;*/
								box-shadow:         0px 0px 2px 1px #999;
								/*box-shadow-top: none;*/
								border-radius: 1px; 
								border: none;
								max-width: 100%;
								font-size: 12pt;
								/*background-color: #eee;*/
								text-align: left;
						}

						textarea.base_para:hover {
								background-color: #fff;
						}
				</style>

				<!-- XXX XXX TODO: for some reason this is causing weird syntax errors -->
				<!--<raw-style css={opts.css} />-->

				<!-- Main editor -->
				<fragment-editor name="fragment_editor" blocks={opts.blocks} />
		</div>

		<script>
				'use strict';
				const events = require('./js/events');

				to_edit(uuid, existing_uuid, existing_content) {
						if (existing_uuid) {
								// This is triggered if the user goes directly from
								// one edit state to another without transitioning
								// into normal mode between
								const normal_payload = {
										uuid: existing_uuid,
										content: existing_content,
								};
								this.opts.send('edit_to_edit', uuid, normal_payload);
						} else {
								this.opts.send('edit', uuid);
						}
				}

				to_split(uuid, content, index) {
						// console.log('sending split', {uuid, content, index});
						this.opts.send('split', {uuid, content, index});
				}

				to_normal(uuid, content) {
						const payload = {
								uuid: uuid,
								content: content,
						};
						this.opts.send('normal', payload);
				}

				this.on('updated', () => {
						// TODO: do diff here
						this.tags.fragment_editor.update()
						events.enhance_editor(this);
				});

				this.on('mount', () => {
						events.activate(this);
				});

				this.on('unmount', () => {
						events.deactivate(this);
				});
		</script>
</document>
