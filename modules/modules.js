module.exports = {
    types: {
        scrollworkspace: {
            path: 'workspace/workspace',
            tagname: 'workspace',
        },
        document: {
            path: 'document/document',
            tagname: 'document',
        },
        tag: {
            path: 'ide/tag',
            tagname: 'tag',
        },
    },
};
