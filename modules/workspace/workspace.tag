<workspace>
		<style scoped>
				li.collection-item {
						cursor: pointer;
				}
		</style>

		<div class="left-pane z-depth-3">
				<div class="left-pane-title">
						<h5>Workspace</h5>
				</div>
				<div class="left-pane-contents">
						<collection>
								<virtual each={item in parent.opts.groups}>
										<collection-item onclick={parent.parent.toggle}>
												{item.collapsed ? '▸' : '▾' } <em>{item.label}s</em>
										</collection-item>
										<virtual if={!item.collapsed}>
												<collection-item each={item.objects} active={active} onclick={parent.parent.parent.activate}>
														&nbsp; &nbsp; &nbsp; {title}
												</collection-item>
										</virtual>
								</virtual>
						</collection>
						<!--
						<collection>
								<collection-item each={parent.opts.objects} active={active} onclick={parent.parent.activate}>
										{title}
								</collection-item>
						</collection>
						-->
				</div>
		</div>
		<div class="right-pane">
        <div class="z-depth-2 editor-decor-default">
            <div id="editor_pane"></div>
        </div>
		</div>
		<script>
				toggle(ev) {
						// TODO not sure why its item -> item here...
						window.log('this is type name', ev.item.item.typename);
						opts.send('toggle', ev.item.item.typename);
				}
				activate(ev) {
						opts.send('activate', ev.item.path);
				}
		</script>
</workspace>
